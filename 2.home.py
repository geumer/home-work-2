from datetime import date, timedelta
def days(n):
    today = date.today()
    for i in range(n):
        today += timedelta(days=5)
        yield today
l = list(days(6))
print(l)
